[ ] **Версия без сервера**

[ ] [Версия с сервером](https://gitlab.com/_reddevel/blazorgame/-/tree/webassembly-aspnet-core-hosted)

Simple blazor game for educate blazor

StepByStep

1) dotnet new blazorwasm -o BlazorGame
2) cd BlazorGame
3) dotnet run

4) dotnet new gitignore

5) Удалил дефолтные страницы

6) Создал папку Components
7) Добавил в эту папку razor компоненты Square и Board

8) Добавил в index.razor компонент Board

9) Добавил простую логику проставления крестиков и ноликов
10) Добавил вторую версию с управлением компонента Square верхним компонентом Board через Props




Пример взят из статьи [Blazor-webassembly-example](https://dev.to/ysflghou/build-tic-tac-toe-game-with-blazor-webassembly-52ih)
